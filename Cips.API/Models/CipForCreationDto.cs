using System;

namespace Cips.API.Models
{
    public class CipForCreationDto
    {
        public decimal Total  { get; set; }
        public string CurrencyCode { get; set; } 
        public string UserEmail { get; set; }
    }
}